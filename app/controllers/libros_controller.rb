class LibrosController < ApplicationController
  before_action :set_libro, only: [:show, :edit, :update, :destroy, :upload, :do_upload, :resume_upload, :update_status, :reset_upload]

  # GET /libros
  # GET /libros.json
  def index
    @libros = Libro.all.order(title: :asc)
  end

  # GET /libros/1
  # GET /libros/1.json
  def show
    # If upload is not commenced or finished, redirect to upload page
    return redirect_to upload_libro_path(@libro) if @libro.status.in?(%w(new uploading))
  end

  # GET /libros/new
  def new
    @libro = Libro.new
  end

  # GET /libros/1/edit
  def edit
  end

  # POST /libros
  # POST /libros.json
  def create
    @libro = Libro.new(libro_params)
    @libro.status = 'new'

    respond_to do |format|
      if @libro.save
        format.html { redirect_to upload_libro_path(@libro), notice: 'Libro creado con éxito.' }
        format.json { render :show, status: :created, location: @libro }
      else
        format.html { render :new }
        format.json { render json: @libro.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /libros/1
  # PATCH/PUT /libros/1.json
  def update
    destroy if params[:delete_upload] == 'sí'

    unless @libro.destroyed?
      respond_to do |format|
        if @libro.update(libro_params)
          format.html { redirect_to @libro, notice: 'Libro was successfully updated.' }
          format.json { render :show, status: :ok, location: @libro }
        else
          format.html { render :edit }
          format.json { render json: @libro.errors, status: :unprocessable_entity }
        end
      end
    end  
  end

  # DELETE /libros/1
  # DELETE /libros/1.json
  def destroy
    @libro.destroy
    respond_to do |format|
      format.html { redirect_to libros_url, notice: 'Libro was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /libros/:id/upload
  def upload

  end

  # PATCH /libros/:id/upload.json
  def do_upload
    unpersisted_libro = Libro.new(upload_params)

    # If no file has been uploaded or the uploaded file has a different filename,
    # do a new upload from scratch
    if @libro.upload_file_name != unpersisted_libro.upload_file_name
      @libro.assign_attributes(upload_params)
      @libro.status = 'uploading'
      @libro.save!
      render json: @libro.to_jq_upload and return

    # If the already uploaded file has the same filename, try to resume
    else
      current_size = @libro.upload_file_size
      content_range = request.headers['CONTENT-RANGE']
      begin_of_chunk = content_range[/\ (.*?)-/,1].to_i # "bytes 100-999999/1973660678" will return '100'

      # If the there is a mismatch between the size of the incomplete upload and the content-range in the
      # headers, then it's the wrong chunk! 
      # In this case, start the upload from scratch
      unless begin_of_chunk == current_size
        @libro.update!(upload_params)
        render json: @libro.to_jq_upload and return
      end
      
      # Add the following chunk to the incomplete upload
      File.open(@libro.upload.path, "ab") { |f| f.write(upload_params[:upload].read) }

      # Update the upload_file_size attribute
      @libro.upload_file_size = @libro.upload_file_size.nil? ? unpersisted_libro.upload_file_size : @libro.upload_file_size + unpersisted_libro.upload_file_size
      @libro.save!

      render json: @libro.to_jq_upload and return
    end
  end

  # GET /libros/:id/reset_upload
  def reset_upload
    # Allow users to delete uploads only if they are incomplete
    raise StandardError, "Action not allowed" unless @libro.status == 'uploading'
    @libro.update!(status: 'new', upload: nil)
    redirect_to @libro, notice: "Upload reset successfully. You can now start over"
  end

  # GET /libros/:id/resume_upload.json
  def resume_upload
    render json: { file: { name: @libro.upload.url(:default, timestamp: false), size: @libro.upload_file_size } } and return
  end

  # PATCH /libros/:id/update_upload_status
  def update_status
    raise ArgumentError, "Wrong status provided " + params[:status] unless @libro.status == 'uploading' && params[:status] == 'uploaded'
    @libro.update!(status: params[:status])
    head :ok
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_libro
    @libro = Libro.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def libro_params
    params.require(:libro).permit(:title, :author, :published_on, :description, :imageurl, :coverurl)
  end

  def upload_params
    params.require(:libro).permit(:upload)
  end
end
