class Libro < ActiveRecord::Base
  # Variables
  LIBRO_STATUSES = %w(new uploading uploaded)

  # Validations
  validates :title, presence: true
  validates :status, inclusion: { in: LIBRO_STATUSES }

  # Paperclip
  has_attached_file :upload, url: "/:basename.:extension"
  validates_attachment :upload, content_type: { content_type: ['application/zip', 'application/pdf'] }

  before_save :save_thumbnail
  def save_thumbnail
    p '*****************************************************'
    p '*****************************************************'
    if Dir.glob(Rails.root.join('public', '*.pdf')).any?
      filename = ''
      Dir.glob(Rails.root.join('public', '*.pdf')) {|file_name|
        filename =  file_name
        image = StorageBucket.files.new(
          key: File.basename(filename),
          body: File.read(Rails.root.join('public', filename)),
          public: true
        )
        image.save
        @hypdf_response = HyPDF.pdfextract(
          Rails.root.join('public', filename),
          user: 'victorcasas.sm@gmail.com',
          password: '12cscv01',
          first_page: 1,
          last_page: 1
        )
        File.open(Rails.root.join('public', filename), 'wb') do |file|
          file.write(@hypdf_response[:pdf])
        end
        pdf = Magick::ImageList.new(Rails.root.join('public', filename))
        pdf.first.write(Rails.root.join('public', filename + '.jpg'))
        image2 = StorageBucket.files.new(
          key: File.basename(filename) + '.jpg',
          body: File.read(Rails.root.join('public', filename + '.jpg')),
          public: true
        )
        image2.save
        File.delete(Rails.root.join('public', filename))
        File.delete(Rails.root.join('public', filename + '.jpg'))
        self.imageurl =   image.public_url
        self.coverurl =   image2.public_url
      }
    end
  end
  
  def to_jq_upload(error=nil)
    {
      files: [
        {
          name: read_attribute(:upload_file_name),
          size: read_attribute(:upload_file_size),
          url: upload.url(:original),
          delete_url: Rails.application.routes.url_helpers.libro_path(self),
          delete_type: "DELETE" 
        }
      ]
    }
  end

  def upload_done?
    status.in? %w(uploaded) 
  end
end
