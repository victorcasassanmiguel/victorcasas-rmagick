class CreateLibros < ActiveRecord::Migration
  def change
    create_table :libros do |t|
      t.string :title, null: false
      t.string :author
      t.date :published_on
      t.text :description
      t.string :status, null: false, index: true
      t.attachment :upload
      t.boolean :visible, default: false, null: false
      t.timestamps null: false
    end
  end
end
