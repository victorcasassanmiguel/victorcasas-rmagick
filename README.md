# Victorcasas-rmagick
PDF Cloud Storage App with Rails 5, Rmagick and Google Cloud Storage (http://victorcasas-rmagick.herokuapp.com) by V��ctor Casas San Miguel.

## Built With
* [Hypdf](https://elements.heroku.com/addons/hypdf) - Your Swiss Army knife for working with PDF
* [Rmagick](https://github.com/rmagick) -  An interface between the Ruby programming language and ImageMagick/GraphicsMagick
* [Google Cloud Storage](https://cloud.google.com/storage/) - Unified object storage for developers and enterprises
* [jQuery File Upload](https://blueimp.github.io/jQuery-File-Upload/) - Upload files Ajax style
* [Bootstrap](http://getbootstrap.com/) - Mobile first front-end framework